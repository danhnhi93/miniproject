<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix'=>'api/v1', 'middleware' => ['jwt.auth']], function () {
    Route::post('loan-amount/register', 'Api\LoanAmountController@register')->name('loan.register');
    Route::post('loan-amount/repayment', 'Api\LoanAmountController@repayment')->name('loan.repayment');
    Route::get('auth/logout', 'Api\UsersController@logout')->name('auth.logout');
});
Route::group(['prefix'=>'api/v1'], function () {
    Route::post('auth/register', 'Api\UsersController@register')->name('auth.register');
    Route::post('auth/login', 'Api\UsersController@login')->name('auth.login');
});