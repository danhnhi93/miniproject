<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use App\User;

class UsersTest extends TestCase
{
    public function testRegister()
    {
        $data = [
            'email' => 'test@gmail.com',
            'name' => 'Test',
            'password' =>'12345678'
        ];

        $response = $this->json('POST', route('auth.register'), $data);
        $response->assertStatus(200);
        $this->assertArrayHasKey('token', $response->json());
        User::where('email', 'test@gmail.com')->delete();
    }

    public function testLogin()
    {
        User::create([
            'name' => 'test',
            'email'=>'test@gmail.com',
            'password' => bcrypt('12345678')
        ]);
        $response = $this->json('POST', route('auth.login'),[
            'email' => 'test@gmail.com',
            'password' => '12345678',
        ]);
        $response->assertStatus(200);
        $this->assertArrayHasKey('token', $response->json());
        User::where('email', 'test@gmail.com')->delete();
    }
}
