<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\ConstantsModel;


class LoanAmountTest extends TestCase
{
    public function testRegister()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');

        $formData = [
            'user_id' => '1',
            'loan_type' => '1',
            'loan_code' => 'BHNDGL22MN',
            'reason' => 'test',
            'duration' => '4',
            'total_fee' => 100000 * ConstantsModel::CONVERT_FEE * 2,// interest_rate = 2
            'repayment_frequency' => '1',
            'interest_rate' => '2',
            'arrangement_fee' => 1000 * ConstantsModel::CONVERT_FEE,
            'status' => ConstantsModel::$STATUS['register'],
            'start_date' => date('Y-m-d'),
            'end_date' => date('Y-m-d', strtotime('+ 4 years')),
        ];

        $this->json('POST', route('loan.register'), $formData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                "data" => $formData,
                "message" => "Register loan amount successfully."
            ]);
    }

    public function testRepayment()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');

        $formData = [
            'loan_amount_id' => '1',
            'payment_type' => 1,//1: credit card : 2: tranfer
            'payment_date' => date('Y-m-d H:i:s'),
            'fee' => 100000,
            'times_pay' => 1,
        ];

        $this->json('POST', route('loan.repayment'), $formData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                "data" => $formData,
                "message" => "Repayment loan amount successfully."
            ]);
    }
}
