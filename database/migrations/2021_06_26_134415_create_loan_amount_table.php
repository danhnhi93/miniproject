<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanAmountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_amount', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unsigned();
            $table->string('loan_code');
            $table->tinyInteger('loan_type');
            $table->text('reason');
            $table->float('duration');
            $table->bigInteger('total_fee');
            $table->tinyInteger('repayment_frequency');
            $table->float('interest_rate');
            $table->bigInteger('arrangement_fee');
            $table->tinyInteger('status');
            $table->date('start_date');
            $table->date('end_date');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_amount');
    }
}
