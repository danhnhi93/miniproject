<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repayment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('loan_amount_id')->unsigned();
            $table->tinyInteger('payment_type');
            $table->dateTime('payment_date');
            $table->bigInteger('fee');
            $table->integer('times_pay');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repayment');
    }
}
