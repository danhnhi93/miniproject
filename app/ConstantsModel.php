<?php

namespace App;

class ConstantsModel
{
    public static $STATUS = [
        'register' => 1,
        'repayment' => 2
    ];
    public static $LOAN_TYPE = [
        'startup' => 1,
        'restructuring' => 2,
        'expand production' => 3,
    ];

    const LENGTH_CODE = 10;
    const CONVERT_FEE = 100;

}
