<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\LoanAmount;
use App\Repayment;
use App\ConstantsModel;

class LoanAmountController extends Controller
{
    public function register(Request $request){
        try {
            $loanAmountTb = new LoanAmount();
            $user = JWTAuth::user();
            $checkRegister = $loanAmountTb->checkRegister($user->id);
            $loan_amount = [];
            $msg = "Loan amount with user " . strtoupper($user->name) . " exist.";
            if (empty($checkRegister)) {
                $loan_amount = [
                    'user_id' => $user->id,
                    'loan_type' => $request->loan_type,//type of loan , please check in ConstantsModel:$LOAN_TYPE
                    'loan_code' => $this->generateRandomString(ConstantsModel::LENGTH_CODE),
                    'reason' => $request->reason,
                    'duration' => $request->duration,
                    'total_fee' => $request->total_fee * ConstantsModel::CONVERT_FEE * $request->interest_rate,
                    'repayment_frequency' => $request->repayment_frequency,
                    'interest_rate' => $request->interest_rate,
                    'arrangement_fee' => $request->arrangement_fee * ConstantsModel::CONVERT_FEE,
                    'status' => ConstantsModel::$STATUS['register'],
                    'start_date' => date('Y-m-d'),
                    'end_date' => date('Y-m-d', strtotime('+' . $request->duration . ' years')),
                ];

                $loanAmountTb->fill($loan_amount);
                $loanAmountTb->save();
                $msg = "Loan amount created successfully";
            }
            return response()->json([
                'status'=> 200,
                'message'=> $msg,
                'data' => !empty($loan_amount) ? $loanAmountTb : $loan_amount
            ]);

        } catch (\Exception $e) {
            return response()->json([
                'status'=> 500,
                'message'=> $e->getMessage()
            ]);
        }
    }

    public function generateRandomString($length)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return strtoupper($randomString);
    }

    public function repayment(Request $request)
    {
        try {
            if (!$request->offsetExists('loan_amount_id')) {
                return response()->json([
                    'status'=> 500,
                    'message'=> "Please input loan amount."
                ]);
            }
            $repaymentTb = new Repayment();
            $checkRepayment = $repaymentTb->checkRepayment($request->loan_amount_id);
            $loanAmountData = LoanAmount::find($request->loan_amount_id);
            $is_flag = true;
            $times_pay = 1;
            $msg = null;
            if (!empty($checkRepayment->count()) && !empty($loanAmountData)) {
                $times_pay = $checkRepayment->times_pay;
                if ($loanAmountData->status == ConstantsModel::$STATUS['repayment']) {
                    $msg = "Loan that’s already been repaid.";
                    $is_flag = false;
                }
                if (($checkRepayment->sum_fee + $request->fee) > $loanAmountData->total_fee) {
                    $msg = "Repayment amount is more than loan amount.";
                    $is_flag = false;
                }
            }

            if ($is_flag) {
                $repayment = [
                    'loan_amount_id' => $request->loan_amount_id,
                    'payment_type' => $request->payment_type,//1: credit card : 2: tranfer
                    'payment_date' => date('Y-m-d H:i:s'),
                    'fee' => $request->fee,
                    'times_pay' => $times_pay
                ];

                $repaymentTb->fill($repayment);
                $repaymentTb->save();
                return response()->json([
                    'status'=> 200,
                    'message'=> "You have refunded this amount for ".$times_pay." time",
                    'data' => $repaymentTb
                ]);

            } else {
                return response()->json([
                    'status'=> 500,
                    'message'=> $msg
                ]);
            }

        } catch (\Exception $e) {
            return response()->json([
                'status'=> 500,
                'message'=> $e->getMessage()
            ]);
        }

    }
}