<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Repayment extends Model
{
    use SoftDeletes;
    protected $table = 'repayment';
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'loan_amount_id', 'payment_type', 'payment_date', 'fee', 'times_pay'
    ];

    public function checkRepayment($loan_amount_id)
    {
         return DB::table('repayment as tb1')
             ->selectRaw('max(tb1.times_pay) as times_pay, sum(tb1.fee) as sum_fee')
             ->join('loan_amount as tb2', 'tb2.id', 'tb1.loan_amount_id')
             ->where('tb2.end_date', '>=', date('Y-m-d'))
             ->where('tb1.loan_amount_id', $loan_amount_id)
             ->whereNull('tb1.deleted_at')
             ->whereNull('tb2.deleted_at')
             ->groupBy('tb1.loan_amount_id')
             ->get();

    }
}
