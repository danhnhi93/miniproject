<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\ConstantsModel;

class LoanAmount extends Model
{
    use SoftDeletes;
    protected $table = 'loan_amount';
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'loan_code', 'loan_type', 'reason', 'duration', 'total_fee', 'repayment_frequency', 'interest_rate', 'arrangement_fee', 'status', 'start_date', 'end_date'
    ];

    public function checkRegister($user_id)
    {
        return self::where(['user_id' => $user_id, 'status' => ConstantsModel::$STATUS['register']])->whereNull('deleted_at')->first();
    }
}
